Helpers Directory
===

This directory contains Handlebar helpers used to add a little logic/flow to you views. Look at `app.js` to see how this module
is incorporated into you app automatically.