/*
  All functions exported in this module will be available for use in your views.
  For example, the formatDate function can be used like:

      {{formatDate myDate}}

  Where myDate is a variable in the context passed into the view.

 */
module.exports = {

  formatDate: function (value, options) {
    // make sure we have a value passed in
    if (value) {
      // the value passed in is a string, so create a new Date
      var dt = new Date(value);
      // format the date as M/D/YYY and return it as a string
      return (dt.getMonth() + 1) + '/' + dt.getDate() + '/' + dt.getFullYear();
    }
    // since we weren't given a value, just return an empty string
    return '';
  }

};