var passport = require('passport'),
    BasicStrategy = require('passport-http').BasicStrategy,
    config = require('../config');

passport.use(new BasicStrategy(
  function(username, password, done) {
    if (username !== config.auth.username || password !== config.auth.password) {
      return done(null, false);
    }
    return done(null, {username: config.auth.username});
  }
));