var http = require('http');

// export a middleware factory that takes a url pattern and options
module.exports = function (pattern, options) {

  // return the actual middleware function 
  return function (req, res, next) {
    // check to see if the requested url matches our pattern
    var matches = req.url.match(pattern);
    if (matches) {
      // make a copy of the current request headers to we can 
      // pass those along 
      var headers = req.headers;
      // remove the host header
      delete headers.host;
      // remove the authorization header so we can change it to what we want it to be
      delete headers.authorization;
      // setup of the params that we pass to the new http proxy
      var params = {
        hostname: options.hostname,
        port: options.port,
        path: options.path + '/' + matches[1],
        auth: options.auth,
        method: req.method,
        headers: headers
      };
      // create a new http request that will do the proxying
      var proxyRequest = http.request(params, function (proxyResponse) {
        // set our response status and headers base on what we got back from the proxy
        res.status(proxyResponse.statusCode).set(proxyResponse.headers);
        // when we get data from the proxy response, just write it out to our response
        proxyResponse.on('data', function (chunk) {
          res.write(chunk, 'binary');
        });
        // when we get an end event on the proxy response, end our response too
        proxyResponse.on('end', function (chunk) {
          res.end();
        });
      });
      // as we get data written to our request, pass it on to our proxy request
      req.on('data', function(chunk) {
        proxyRequest.write(chunk, 'binary');
      });
      // if our incoming request ends, then end our proxy request too
      req.on('end', function() {
        proxyRequest.end();
      });
    } else {
      return next();
    }
  };

};