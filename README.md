Revved To Do
===============

Getting Started
---------------

Make sure `grunt-cli` and `node-inspector` are installed:

    npm install grunt-cli -g
    npm install node-inspector -g

Install all application dependencies:

    npm install

To run the application and have it automatically relaunch when a file has been added, deleted, or changed:

    grunt run

To debug the application using node inspector:

    grunt debug


Project Structure
-----------------

    config/              -- configuration json files for different environments
    helpers/             -- helper functions for Handlebars views
    lib/                 -- local modules that belong in any of the other directories
    models/              -- Mongoose database models
    public/              -- static files
      css/               -- css stylesheets 
      img/               -- image assets
      js/                -- client side javascript
        lib/             -- 3rd party client side javascript libraries
    routes/              -- route handlers or controllers
    views/               -- Handlebars template files
      layouts/           -- layouts used by other templates
      partials/          -- partials that can be include in other templates
