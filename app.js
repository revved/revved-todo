// require application dependencies
var fs = require('fs'),
    path = require('path'),
    express = require('express'),
    exphbs  = require('express3-handlebars'),
    passport = require('passport'),
    package = require('./package.json'),
    debug = require('debug')('todo'),
    proxy = require('./lib/proxy'),
    config = require('./config');

// create a new express app and initialize some variables
var app = express(),
    port = process.env.PORT || 8080,
    env = process.env.NODE_ENV || 'development';

debug('initializing ' + package.name + ' (' + env + ')');

// expose application properties to views
app.locals({
  app: {
    name: package.name,
    version: package.version,
    env: env
  }
});

// set Handlebars as the view engine
debug('... initializing handlebars view engine');
var hbs = exphbs.create({
  defaultLayout: 'main',            // this is the default layout
  helpers: require('./helpers')     // automatically registers module functions has helpers
});
// add handlebars as a view engine
app.engine('handlebars', hbs.engine);
// set handlebars as the default view engine
app.set('view engine', 'handlebars');
// tell the view engine where it can find the templates
app.set('views', __dirname + '/views');

// allow the application to sit behind a proxy
app.enable('trust proxy');

// initialize middleware
debug('... initializing middleware');

// proxy requests to the api
app.use(proxy('/proxy/(.*)', config.apiProxy));

// handle requests for favicon
app.use(express.favicon('public/favicon.ico'));
// log all requests
app.use(express.logger());
// auto gzip responses
app.use(express.compress());
// server up static files from the public directory
app.use(express.static(__dirname + '/public'));
// parse cookies and add values to the request
app.use(express.cookieParser());
// parse the request body and add values to the request
app.use(express.bodyParser());
// passport middleware to inspect the request for authentication
app.use(passport.initialize());
// route requests
app.use(app.router);
// handle any errors that may have happened along the way
app.use(express.errorHandler({dumpExceptions: true, showTrace: true}));

// intialize authentication config
debug('... intializing authentication');
require('./lib/auth');

// initialize application routes
debug('... initializing routes');
require('./routes').init(app);

// start server
app.listen(port);
debug('listening on port ' + port);