/*
  This module handles connecting to the database and registration of the
  created schemas as models.
 */
var mongoose = require('mongoose'),
    config = require('../config');

// constructor for object to house models
var Models = function() {

  // if we aren't already connected to the database, connect using the 
  // 'db.uri' value from our config file
  if (mongoose.connection.readyState === 0) {
    mongoose.connect(config.db.uri);
  }

  /*
    Add mongoose models to "this" here.

    'mongoose.model' takes a name and a schema and creates a new Model 
    from it. Once you register a model as we have below you can reference it
    again by calling:

      var myModel = mongoose.model('MyModel');

    where 'MyModel' is the name you passed into mongoose.model before.

    here we export the model as a property of the module so we can conviently do this 
    from within our application: 

    var models = require('./models');
    var newTask = new models.Task();
  */
  this.Task = mongoose.model('Task', require('./task'));
}

module.exports = new Models();
