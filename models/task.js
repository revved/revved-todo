/*
  Allows us to define the structure, validation, and logic around our
  Task model.
 */

var mongoose = require('mongoose'),
    validator = require('validator'),
    util = require('util');

// create a new mongoose Schema with appropriate attributes
var TaskSchema = new mongoose.Schema({
  // each task has a required description that is a string
  description: { type: String, required: true },
  // when a task is created, timestamp it with the current date/time
  creation_date: { type: Date, 'default': Date.now },
  // if now due date is given, default it to the current date/time
  due_date: { type: Date, 'default': Date.now },
  // default the complete attribute to false
  complete: { type: Boolean, 'default': false },
  // url for this task for absolutely no reason
  urls: [String],
  email: { type: String }
});

// this tells mongoose how it should handle returning a documents
// attributes when toJSON is called on the object.
TaskSchema.set('toJSON', {
  // tell mongoose to return virtual getters 
  virtual: true,
  getters: true,
  // allows us to make some final tweaks to the object before 
  // it is return from a call to toJSON. 
  // the arguments are: 
  //  doc (the original mongoose document)
  //  ret (the plain object value before we transform it)
  //  options (options passed into the schema or inline)
  transform: function (doc, ret, options) {
    // remove the version attribute from the object
    delete ret.__v;
    // remove the _id attribute from the object
    // NOTE: the id attribute will still be included because
    // that is a built in virtual
    delete ret._id;
    // return the transformed plain object (not mandatory but is good practice)
    return ret;
  }
});

TaskSchema.path('urls').validate(function (value, respond) {
  if (!util.isArray(value)) return respond(false);
  try {
    for (var i = 0; i < value.length; i++) {
      validator.check(value[i]).isUrl();
    }
    respond(true);
  } catch (e) {
    respond(false);
  }
}, 'urls must be an array of urls');

TaskSchema.path('email').validate(function (value, respond) {
  try {
    validator.check(value).isEmail();
    respond(true);
  } catch (e) {
    respond(false);
  }
}, 'email must be an email');

// creates a non-persisted attribute of a Task called 'past_due'
TaskSchema.virtual('past_due')
  // add a getter to calculate and return the 'past_due' value
  .get(function () {
    // return true if the due_date is set and is less than the current date
    return this.due_date && this.due_date < new Date();
  });
  // NOTE: we could have also created a setter but 'past_due' is read only so we didn't

// create static method on the Task model to fetch a sorted list of tasks
TaskSchema.statics.listSorted = function (cb) {
  // get a reference to the model registered as 'Task' in mongoose
  this.model('Task')
    // get all tasks 
    .find({})
    // sort them ascending by complete then ascending by due_date
    .sort({complete: 1, due_date: 1})
    // execute the query and let the method callback handle the response
    .exec(cb);
};

// export the schema for use within our application
module.exports = TaskSchema;