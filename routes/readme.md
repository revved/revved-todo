Routes Directory
===

This directory houses the routing and route callbacks (or middleware) 
used to handle incoming requests and provide a response.