/*
  Contains middleware that will handle requests to the site's home page
 */

var models = require('../models');

// export a middleware to display a list of sorted tasks
exports.index = function (req, res, next) {
  // use the listSorted static method on the Task model to fetch a sorted list
  models.Task.listSorted(function (err, tasks) {
      // if an error occurs, let the errorHandler middleware deal with it later
      if (err) return next(err);
      // render the 'home' view and pass in the page title and an array of tasks
      res.render('home', {title: 'Revved Todo', tasks: tasks});
    });
};