/*
  Contains middleware that will handle requests to the site's task api
 */

var models = require('../../models'),
    debug = require('debug')('todo:api');

// export middleware to return a json list of sorted tasks
exports.list = function (req, res, next) {
  debug('running tasks.list');
  // use the listSorted static method on the Task model to fetch a sorted list
  models.Task.listSorted(function (err, tasks) {
      // if an error occurs, let the errorHandler middleware deal with it later
      if (err) return next(err);
      // return the JSON representation of the task list on the response
      res.json(200, tasks);
    });
};

// export middleware to return a json task
exports.get = function (req, res, next) {
  debug('running tasks.get');
  // get the task id that was passed in on the URL
  var taskId = req.params.id;
  // find the task with the requested ID
  models.Task.findById(taskId, function (err, task) {
    // if an error occurs, let the errorHandler middleware deal with it later
    if (err) return next(err);
    // return the JSON representation of the task on the response
    res.json(200, task);
  });
};

// export middleware to update an existing task
exports.put = function (req, res, next) {
  // get the task id that was passed in on the URL
  var taskId = req.params.id;
  // get the body of the request to update the task
  var taskObj = req.body;
  // find the task with the requested ID and update it with the given data
  models.Task.findByIdAndUpdate(taskId, taskObj, function (err, task) {
    // if an error occurs, let the errorHandler middleware deal with it later
    if (err) return next(err);
    // return the JSON representation of the task that was updated on the response
    res.json(200, task);
  });
};

// export middleware to create a new task
exports.post = function (req, res, next) {
  // get the body of the request to create the task
  var task = req.body;
  // create a new task from the request body
  models.Task.create(task, function (err, task) {
    // if an error occurs, let the errorHandler middleware deal with it later
    if (err) return next(err);
    // tell the api client the url of the newly created task
    res.header('Location', '/api/tasks/' + task.id);
    // return the JSON representation of the new task with a 201 status
    res.json(201, task);
  });
};

// export middleware to delete an existing task
exports.del = function (req, res, next) {
  // get the task id that was passed in on the URL
  var taskId = req.params.id;
  // find the task with the requested ID and remove it from the database
  models.Task.findByIdAndRemove(taskId, function (err) {
    // if an error occurs, let the errorHandler middleware deal with it later
    if (err) return next(err);
    // return a success object to let the client know it worked
    res.json(200, {success: true});
  });
};