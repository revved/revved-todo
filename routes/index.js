/*
  Maps express application routes to middleware to handle the requests.
  See 'app.js' to see how it is used.
 */
var passport = require('passport');

exports.init = function (app) {

  // require the home middleware module
  var home = require('./home');
  // map home.index to all incoming requests at the site's root
  app.all('/', home.index);

  // require the task api middleware module
  var taskApi = require('./api/tasks');
  // map GET requests to /api/tasks to the taskApi.list middleware
  app.get('/api/tasks', passport.authenticate('basic', {session: false}), taskApi.list);
  // map POST requests to /api/tasks to the taskApi.post middleware
  app.post('/api/tasks', passport.authenticate('basic', {session: false}), taskApi.post);
  // map GET requests to /api/tasks/:id, where :id is the task ID, to the taskApi.get middleware
  app.get('/api/tasks/:id', passport.authenticate('basic', {session: false}), taskApi.get);
  // map PUT requests to /api/tasks/:id, where :id is the task ID, to the taskApi.put middleware
  app.put('/api/tasks/:id', passport.authenticate('basic', {session: false}), taskApi.put);
  // map DELETE requests to /api/tasks/:id, where :id is the task ID, to the taskApi.del middleware
  app['delete']('/api/tasks/:id', passport.authenticate('basic', {session: false}), taskApi.del);



};