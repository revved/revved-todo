Config Directory
===

This directory contains json files for configuring your application for specific environments.
By default, your application will use `development.json` unless the `NODE_ENV` variable is set.