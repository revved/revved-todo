var Handlebars = require('../lib/handlebars.runtime');
Handlebars.registerHelper(require('../../../helpers'));

var taskTemplate = Handlebars.template(require('../../../views/partials/task'));

module.exports = {

  load: function (cb) {
    $.ajax({
        url: '/proxy/tasks',
        method: 'get',
        contentType: 'application/json',
        cache: false,
        success: function (tasks, status, xhr) {
          // on success update the table of tasks
          var html = '';
          for (var i = 0; i < tasks.length; i++) {
            html += taskTemplate(tasks[i]);
          }
          $('#task-list').html(html);
          return cb();
        },
        error: function (xhr, status, error) {
          var response = JSON.parse(xhr.responseText);
          return cb(response.error);
        }
      });
  },

  add: function (data, cb) {
    $.ajax({
        url: '/proxy/tasks',
        method: 'post',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(data),
        success: function (data, status, xhr) {
          return cb();
        },
        error: function (xhr, status, error) {
          var response = JSON.parse(xhr.responseText);
          return cb(response.error);
        }
      });
  },

  complete: function (id, cb) {
    $.ajax({
      url: '/proxy/tasks/' + id,
      method: 'put',
      contentType: 'application/json',
      dataType: 'json',
      data: '{"complete": true}',
      success: function (data, status, xhr) {
        return cb();
      },
      error: function (xhr, status, error) {
        var response = JSON.parse(xhr.responseText);
        return cb(response.error);
      }
    });
  },

  remove: function (id, cb) {
    $.ajax({
      url: '/proxy/tasks/' + id,
      method: 'delete',
      dataType: 'json',
      success: function (data, status, xhr) {
        return cb();
      },
      error: function (xhr, status, error) {
        var response = JSON.parse(xhr.responseText);
        return cb(response.error);
      }
    });
  }

};