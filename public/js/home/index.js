var tasks = require('./tasks');

$(document).ready(function () {
  initForm();
  initButtons();
});

function initButtons() {
  // when the complete buttons are clicked, call completeTask
  $('button.complete-task-btn').on('click', function (event) {
    var btn = $(event.target);
    // get the task ID from the data-task-id on the element
    tasks.complete(btn.data('task-id'), function (err) {
      if (err) return alert(err.message);
      // reload all the tasks
      tasks.load(function (err) {
        initButtons();
      });
    });
  });
  // when the delete buttons are clicked, call deleteTask
  $('button.delete-task-btn').on('click', function (event) {
    if (confirm('Are you sure?')) {
      var btn = $(event.target);
      // get the task ID from the data-task-id on the element
      tasks.remove(btn.data('task-id'), function (err) {
        if (err) return alert(err.message);
        // reload all the tasks
        tasks.load(function (err) {
          initButtons();
        });
      });
    }
  });
}

function initForm() {
  // add a submit event listener of the task form
  $('#add-task-form').on('submit', function (event) {
    // make sure you stop the form from submitting since we handled it through Ajax
    event.preventDefault();
    // when the form is submitted...
    var form = $(event.target);
    // build a task object
    var data = {
      description: form.find('input[name=description]').val(),
      due_date: form.find('input[name=due_date]').val()
    };
    // and call add task to create it
    tasks.add(data, function (err) {
      if (err) return alert(err.message);
      // reload all the tasks
      tasks.load(function (err) {
        initButtons();
      });
    });
  });
}