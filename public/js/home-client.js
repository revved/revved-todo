// create client side helper to format the date
Handlebars.registerHelper('formatDate', function (value, options) {
  if (value) {
    var dt = new Date(value);
    return (dt.getMonth() + 1) + '/' + dt.getDate() + '/' + dt.getFullYear();
  }
  return '';
});

/*
  Load the tasks by calling our API through Ajax and displaying using a client side template
 */
function loadTasks() {
  // build the template
  var taskTemplateSource =
    '<tr class="task {{#if past_due}}past-due{{/if}} {{#if complete}}complete{{/if}}">' +
    '<td>{{#if complete}}&#10004;{{else}}<button class="complete-task-btn" data-task-id="{{id}}">Complete</button>{{/if}}</td>' +
    '<td><strong>{{description}}</strong></td><td>{{formatDate due_date}}</td>' +
    '<td><button class="delete-task-btn" data-task-id="{{id}}">Delete</button></td>' +
    '</tr>';
  // compile the template for faster execution
  var taskTemplate = Handlebars.compile(taskTemplateSource);
  // call our api endpoint
  $.ajax({
    url: '/proxy/tasks',
    method: 'get',
    contentType: 'application/json',
    cache: false,
    success: function (tasks, status, xhr) {
      // on success update the table of tasks
      var html = '';
      for (var i = 0; i < tasks.length; i++) {
        html += taskTemplate(tasks[i]);
      }
      $('#task-list').html(html);
      // hook up the button event handlers
      initButtons();
    },
    error: function (xhr, status, error) {
      // on error display a simple alert message
      var response = JSON.parse(xhr.responseText);
      alert(response.error.message);
    }
  });
}

/*
  Initialize our task buttons to call the appropriate functions when clicked
 */
function initButtons() {
  // when the complete buttons are clicked, call completeTask
  $('button.complete-task-btn').on('click', function (event) {
    var btn = $(event.target);
    // get the task ID from the data-task-id on the element
    completeTask(btn.data('task-id'));
  });
  // when the delete buttons are clicked, call deleteTask
  $('button.delete-task-btn').on('click', function (event) {
    var btn = $(event.target);
    // get the task ID from the data-task-id on the element
    deleteTask(btn.data('task-id'));
  });
}

/*
  Creates a new task by calling POST /proxy/tasks
 */
function addTask(data) {
  $.ajax({
    url: '/proxy/tasks',
    method: 'post',
    contentType: 'application/json',
    dataType: 'json',
    data: JSON.stringify(data),
    success: function (data, status, xhr) {
      // on success reload the tasks
      loadTasks();
    },
    error: function (xhr, status, error) {
      // on error display a simple alert message
      var response = JSON.parse(xhr.responseText);
      alert(response.error.message);
    }
  });
}

/*
  Completes a task by calling PUT /proxy/tasks/:id
 */
function completeTask(id) {
  $.ajax({
    url: '/proxy/tasks/' + id,
    method: 'put',
    contentType: 'application/json',
    dataType: 'json',
    data: '{"complete": true}',
    success: function (data, status, xhr) {
      // on success reload the tasks
      loadTasks();
    },
    error: function (xhr, status, error) {
      // on error display a simple alert message
      var response = JSON.parse(xhr.responseText);
      alert(response.error.message);
    }
  });
}

/*
  Deletes a task by calling DELETE /proxy/tasks/:id
 */
function deleteTask(id) {
  $.ajax({
    url: '/proxy/tasks/' + id,
    method: 'delete',
    dataType: 'json',
    success: function (data, status, xhr) {
      // on success reload the tasks
      loadTasks();
    },
    error: function (xhr, status, error) {
      // on error display a simple alert message
      var response = JSON.parse(xhr.responseText);
      alert(response.error.message);
    }
  });
}

// when the document is loaded...
$(document).ready(function () {
  // initialize the button event handlers
  initButtons();
  // add a submit event listener of the task form
  $('#add-task-form').on('submit', function (event) {
    // when the form is submitted...
    var form = $(event.target);
    // build a task object
    var data = {
      description: form.find('input[name=description]').val(),
      due_date: form.find('input[name=due_date]').val()
    };
    // and call addTask to create it
    addTask(data);
    // make sure you stop the form from submitting since we handled it through Ajax
    event.preventDefault();
  });
});